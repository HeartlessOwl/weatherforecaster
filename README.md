Application was created on PYTHON3 Language and requires Pillow and Requests libraries installed in order to function properly.

1) Open up the terminal and write down next commands:
	- pip install Requests
	- pip install Pillow
2) Application was originally written on Linux OS without using any environment
3) In order to launch application - ensure that PC has a stable internet connection and run the file "Weather.py"
